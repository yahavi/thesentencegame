var app = require(__dirname + "/app.js");

// ===Constants===
/*jshint esnext: true */
const REDIS_SERVER_IP = "192.168.0.17";
const GAME_STORY      = "%gameStory";     // List
const GAME_SESSIONS   = "%gameSessions";  // List
const GAMES           = "%games";         // Set
const PLAYER_SESSION  = "%playerSession"; // String
const MAX_STORY_SIZE  = "%storySize";     // Integer
const CURRENT_PLAYER  = "%currentPlayer"; // Integer

var _redis = require("redis");
var redis = _redis.createClient({host : REDIS_SERVER_IP});


/*
 * Data structures:
  * for each game:
  * gameSessions    : Array[player1SessionId, player2SessionId,...] - Unique sessions
  * gameStory       : Array[sentence1, sentence2,...]
  * maxStorySize    : Number
  * currentPlayer   : Number
 *
  * for each player:
  * playerSession : playerName - Each players have one object
 */

module.exports = {
    newGame : function(gameName, maxStorySize, player){
        "use strict";

        this.addPlayer(gameName, player);

        var maxStorySizeKey = gameName + MAX_STORY_SIZE;
        redis.set(maxStorySizeKey, maxStorySize, _redis.print);

        var currentPlayerKey = gameName + CURRENT_PLAYER;
        redis.set(currentPlayerKey, 0, _redis.print);

        redis.sadd([GAMES, gameName], _redis.print);

    },

    addPlayer : function(gameName, player){
        "use strict";
        var gameSessionsKey = gameName + GAME_SESSIONS;
        var playerSessionKey = PLAYER_SESSION; // No gameName needed

        redis.rpush([gameSessionsKey, player.sessionID], _redis.print);
        redis.set(playerSessionKey, player.playerName, _redis.print);
    },

    addSentence : function(gameName, sentence, nextPlayerIdx){
        "use strict";
        var gameStoryKey = gameName + GAME_STORY;
        var currentPlayerKey = gameName + CURRENT_PLAYER;

        redis.rpush([gameStoryKey, sentence], _redis.print);
        redis.set(currentPlayerKey, nextPlayerIdx, _redis.print);
    },

    removePlayer : function(gameName, sessionId){
        "use strict";
        var gameSessionsKey = gameName + GAME_SESSIONS;
        var playerSessionKey = PLAYER_SESSION; // No gameName needed

        redis.lrem(gameSessionsKey, 0, sessionId, _redis.print);
        redis.del(playerSessionKey, _redis.print);
    },

    removeGame : function(gameName){
        "use strict";
        var gameSessionsKey = gameName + GAME_SESSIONS;
        var gamesListKey = GAMES;
        redis.lrange(gameSessionsKey, 0, -1, (error, sessionIds)=>{
            if (error) {
                console.error("Unable to retrieve " + gameName + " from redis");
            } else {
                sessionIds.forEach((sessionId)=>{
                    redis.del(sessionId, _redis.print);
                });
                redis.del(gameSessionsKey, _redis.print);
                redis.srem(gamesListKey, gameName, _redis.print);
            }
        });
    },

    /**
     * Return a games object
     */
    restoreAll : function(games, startServerCbk){
        "use strict";
        //var games = [];
        console.log("restoreAll");
        redis.smembers(GAMES, getGameNamesCbk.bind
                        ({startServerCbk : startServerCbk,
                          games          : games}));
    },
};

var getGameNamesCbk = function(error, gameNames){
    "use strict";
    if (error) {
        console.error("Error in retrieving games from redis");
        this.startServerCbk();
    } else {
        if (0 === gameNames.length){
            console.log("No games stored in redis");
            this.startServerCbk();
        } else {
            console.log(gameNames);
            var startServerCbk = this.startServerCbk;
            var games = this.games;
            startServerCbk.gamesLeft = gameNames.length;
            gameNames.forEach((gameName)=>{
                var maxStorySizeKey = gameName + MAX_STORY_SIZE;
                redis.get(maxStorySizeKey, getMaxGameSizeCbk.
                bind({startServerCbk   : startServerCbk,
                      gameName         : gameName,
                      games            : games}));

            });
        }
    }
};

var getMaxGameSizeCbk = function(error, storySize){
    "use strict";
    if (error) {
        console.error("Can't retrieve maxStorySize: " + error);
        this.startServerCbk.gamesLeft --;
        if (0 === this.startServerCbk.gamesLeft){
            this.startServerCbk();
        }
    } else {
        var game = new app.Game(storySize);
        this.games[this.gameName] = game;

        var currentPlayerKey = this.gameName + CURRENT_PLAYER;
        redis.get(currentPlayerKey, getCurrentPlayerCbk.bind({
                                    startServerCbk : this.startServerCbk,
                                    game           : game,
                                    gameName       : this.gameName}));
    }
};

var getCurrentPlayerCbk = function(error, currentPlayer){
    "use strict";
    if (error) {
        console.error("Can't retrieve getCurrentPlayerCbk: " + error);
        this.startServerCbk.gamesLeft --;
        if (0 === this.startServerCbk.gamesLeft){
            this.startServerCbk();
        }
    } else {
        this.game.currPlayer = currentPlayer;
        var gameStoryKey = this.gameName + GAME_STORY;
        redis.lrange(gameStoryKey, 0, -1, getStoryCbk.bind({
                     startServerCbk : this.startServerCbk,
                     game           : this.game,
                     gameName       : this.gameName}));
    }
};

var getStoryCbk = function(error, story){
    "use strict";
    if (error) {
        console.error("Can't retrieve story: " + error);
        this.startServerCbk.gamesLeft --;
        if (0 === this.startServerCbk.gamesLeft){
            this.startServerCbk();
        }
    } else {
        var game = this.game;
        game.story = [];
        story.forEach((sentence)=>{
            game.story.push(sentence);
        });
        var gameSessionsKey = this.gameName + GAME_SESSIONS;
        redis.lrange(gameSessionsKey, 0, -1, getGameSessionsCbk.bind({
                     startServerCbk : this.startServerCbk,
                     game           : this.game}));
    }
};

var getGameSessionsCbk = function(error, sessions){
    "use strict";
    if (error){
        console.error("Can't retrieve game sessions" + error);
        this.startServerCbk.gamesLeft--;
        if (0 === this.startServerCbk.gamesLeft){
            this.startServerCbk();
        }
    } else {
        this.game.players = [];
        this.game.playersLeft = sessions.length;
        if (0 === this.game.playersLeft){
            this.startServerCbk.gamesLeft--;
            if (0 === this.startServerCbk.gamesLeft){
                this.startServerCbk();
            }
        }
        for (var session in sessions){
            if (sessions.hasOwnProperty(session)){
                var playerSessionKey = PLAYER_SESSION;
                redis.get(playerSessionKey, getPlayerNameCbk.bind({
                          startServerCbk : this.startServerCbk,
                          game           : this.game,
                          playerIdx      : session,
                          session        : sessions[session]}));
            }
        }
    }
};

var getPlayerNameCbk = function(error, playerName){
    "use strict";
    if (error){
        console.error("Can't retrieve player name");
    } else {
        var player = new app.Player(this.session, playerName);
        this.game.players.splice(this.playerIdx, 0, player);
    }
    this.game.playersLeft --;
    if (0 === this.game.playersLeft){
        delete this.game.playersLeft;
        this.startServerCbk.gamesLeft --;
        if (0 === this.startServerCbk.gamesLeft){
            this.startServerCbk();
        }
    }
};
