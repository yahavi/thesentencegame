***Availability - "The Sentence Game"***
========================================
Name: Yahav Itzhak

Username: yahavi

ID: 200615524

--


Name: Valeriya Zelikovich

Username: valeriya

ID: 332581172


General configuration information
=================================
Two VMs in Bluemix

Node0-Public-IP: 169.50.102.45

Node0-Private-IP: 192.168.0.16

Node0-url: http://thesentencegame.tk/


Node1-Public-IP: 169.50.103.253

Node1-Private-IP: 192.168.0.17

Node1-url: None


Files
=====
**index.html** - The main page. The client can 
create a new game or join an existing game.

**game.html** - The game. the player can send sentences in his turn and 
see the whole story in the end of the game. Contains an events log.

**app.js** - The server main logic.

**redis-driver.js** - Manages the storage with minimal I/O needed.

**heartbeat.sh** - A script on node1 that keeps node0 alive.


Part 1: Kill API
================
Usage: "POST 169.50.102.45/kill" or 
"POST https://thesentencegame.tk/kill" or 
"POST thesentencegame.tk/kill", etc.

* It may take up to 1 minute.

Behind the scenes:

1. Node0 get the kill command and kill himself by shutting down the server.

2. Node1 send a ping every 5 seconds. If the ping command return with
timeout, it will infer that the machine is down.

3. Node1 will send a "start" command with OpenStack Nova.

4. Cron will start the application with [forever](https://www.npmjs.com/package/forever).

Any data that been committed in the storage will be saved.
There still can be a scenario of data loss: After sending the kill command,
the player can't know that the server fell, so his/her sentence will be lost.


Part 2: The Sentence Game
=========================


***High Level Design***

Client connect to the server with his browser. The server return him
index.html. Then, the player can start a new game or join a new one.

New Game - The player required to fill up the game's name, game's length and player's name.

Join Game - The player required to fill only its name and click on the game button. 

After the game start, the player will ask the server every 3 seconds
whether its his turn or not.

* Every important message from the server will be written in the event log.

* The game uses an array of games in RAM in order to manage all.

* Every player operation will initialize a timeout function of 2 minutes.
After 2 minutes of no response from current player, 
the server deletes the player and advances to the next player.


Part 3: Storage
===============
We divide the sessions information and the games information to two
different parts. They both being stored in node1, with *redis* technology.
The sessions information, i.e the sessionID is being stored automatically
in the redis storage on node1. 
The games information also stored in node1 with *redis*, but we've built
a driver 'redis-driver.js' in order to save/restore them wisely. 
In order to achieve maximum consistency and speed, every action in the game 
will lead to a small single storage operation. It will save only the delta
and not the complete games information. 

Part 4: Certification
=====================
As instructed we used LetsEncrypt. 
As IBM doesn't supply its VMs with a domain, we looked for a free 
domain and found [Dot-TK](www.dot.tk).
The domain name is: http://thesentencegame.tk/

We asked for a certification to our domain from LetsEncrypt servers 
by the [LetsEncrypt-CLI](https://github.com/Daplie/letsencrypt-cli).

LetsEncrypt-Express responsible to handle all server requests.

