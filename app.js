// ===Requirements===
var express = require('express');
var cfenv = require('cfenv');
var session = require('express-session');
var uuid = require('uuid');
var child_process = require("child_process");
var redis_driver = require(__dirname + "/redis-driver");
var redisStore = require('connect-redis')(session);
var LEX = require('letsencrypt-express');
var path = require('path');

// ===Constants===
/*jshint esnext: true */
const INDEX_HTML_PATH = __dirname + '/public/index.html';
const GAME_HTML_PATH = __dirname + "/public/game.html";
const PLAYER_TIME_OUT_MILLISEC = 120000; // 2 Minutes

// ===Globals===
var games = [];
/*
 games =
 {
    story : [sentence1, sentence2,...,sentenceN];
    players : [{sessionID1 : playerName1},...];
    maxStorySize : X;
    currPlayer = Y;
 }
 */


// create a new express server with session management
var app = express();
app.use(express.static(path.join(__dirname, 'public')));
var lex = LEX.create({
    configDir: require('os').homedir() + '/letsencrypt/etc',
    approveRegistration: (hostname, cb)=> { // leave `null` to disable automatic registration
        // Note: this is the place to check your database to get the user associated with this domain
        "use strict";
        cb(null, {
            domains: [hostname],
            email: "ynvserver@gmail.com",
            agreeTos: true
        });
    }
});

lex.onRequest = app;

app.use(session({
    genid: ()=> {
        "use strict";
        return uuid.v4(); // use UUIDs for session IDs
    },
    store: new redisStore({ host: "192.168.0.17", port: 6379}), // Private IP for a faster intranet communication
    secret: 'WTFisThatPassword',
    resave: true,
    saveUninitialized: true
}));


// start server on the specified port and binding host
redis_driver.restoreAll(games, ()=>{
    "use strict";

    lex.listen([80],[443, 5001], ()=>{

        for (var gameName in games){
            if (games.hasOwnProperty(gameName)){
                var game = games[gameName];
                var players = game.players;
                if (players){
                    var currPlayer = players[game.currPlayer];
                    console.log("Setting timeout to " + currPlayer.playerName);
                    initTimeout(gameName, currPlayer);
                }
                console.log(game);
            }
        }
        console.log("server starting on " + [80], [443, 5001]);
    });

    app.get('/', LEX.createAcmeResponder(lex, (req, res)=>{
        console.log("sessionID = " + req.sessionID);
        var session = req.session;
        var playerName = session.playerName;
        var playerExists = false;
        if (playerName){ // The client have a cookie, search for it.
            for (var iGame in games){
                if (games.hasOwnProperty(iGame) &&
                    -1 !== getPlayerIdx(games[iGame].players, req.sessionID)){
                    // Player found
                    sendHtml(GAME_HTML_PATH, res);
                    playerExists = true;
                    break;
                }

            }
        }

        if (!playerExists) {
            res.sendFile(INDEX_HTML_PATH);
        }
    }));

    /**
     * Get the game.html path
     */
    app.get('/game', LEX.createAcmeResponder(lex, (req, res)=>{
        console.log("sessionID = " + req.sessionID);
	res.sendFile(GAME_HTML_PATH);
    }));

    /**
     * Get a list of games
     */
    app.get('/games', LEX.createAcmeResponder(lex, (req, res)=>{
        console.log(JSON.stringify(Object.keys(games)));
        res.send(JSON.stringify(Object.keys(games)));
    }));

    /**
     * Start a new game
     */
    app.post('/newgame', LEX.createAcmeResponder(lex, (req, res)=>{
        var body = '';

        req.on('data', (data)=>{
            body += data;
        });

        req.on('end', ()=>{
            var data = JSON.parse(body);
            var gameName = data.gameName;
            if (games[gameName]){
                res.sendStatus(201);
                res.end();
                return;
            }

            var maxStorySize = data.maxStorySize;
            var playerName = data.playerName;

            var sess = req.session;
            sess.gameName = gameName;
            sess.playerName = playerName;

            var game = games[gameName] = new Game(maxStorySize);

            var player = new Player(req.sessionID, playerName);

            // Send the game details to the repo
            redis_driver.newGame(gameName, maxStorySize, player);
            game.players = [player];
            game.story = [];
            initTimeout(gameName, player);

            res.sendStatus(200);
            res.end();
        });
    }));

    /**
     * Join a game
     */
    app.post('/joingame', LEX.createAcmeResponder(lex, (req, res)=>{
        var body = '';

        req.on('data', (data)=>{
            body += data;
        });

        req.on('end', ()=>{
            var joinNameObj = JSON.parse(body);
            var gameName = joinNameObj.gameName;
            var playerName = joinNameObj.playerName;
            var currGame = games[gameName];
            var session = req.session;

            if (!currGame){ // Game not exists
                res.sendStatus(201);
                res.end();
                return;
            }
            var player = new Player(req.sessionID, playerName);
            currGame.players.push(player);
            session.gameName = gameName;
            session.playerName = playerName;

            redis_driver.addPlayer(gameName, player);
            res.sendStatus(200);
            res.end();
        });
    }));

    /**
     * Check if it's the client's turn.
     * If it's his turn, send him the last sentence.
     */
    app.get('/waitforturn', LEX.createAcmeResponder(lex, (req, res)=>{
        var sess = req.session;
        var gameName = sess.gameName;
        if (!games || !games[gameName]){
            res.status(404);
            res.sendFile(INDEX_HTML_PATH);
            return;
        }

        var game = games[gameName];
        var players = game.players;

        var playerIndex = getPlayerIdx(players, req.sessionID);
        if (!players[playerIndex]){ // Player deleted
            res.status(404);
            res.sendFile(INDEX_HTML_PATH);
            return;
        }
        var story = game.story;
        var storyLength = story.length;

        if (storyLength === Number(game.maxStorySize)){ // Game over
            res.writeHead(201, {"Content-Type": "text/plain"});
            res.end(makeStory(story));
        } else if (playerIndex === Number(game.currPlayer)){ // Player's turn
            console.log("OK, play");
            var lastSentence = storyLength > 0 ? story[storyLength - 1] : "";
            res.writeHead(200, {"Content-Type": "text/plain"});
            res.end(lastSentence);
        } else { // Not player's turn
            console.log("Can't play since " + playerIndex + " !== " + game.currPlayer);
            res.sendStatus(204).end();
        }
    }));

    /**
     * Client post a sentence
     */
    app.post('/postsentence', LEX.createAcmeResponder(lex, (req, res)=>{
        var sentence = '';
        var sess = req.session;
        var gameName = sess.gameName;

        var game = games[gameName];
        if (!game){
            res.status(404);
            res.sendFile(INDEX_HTML_PATH);
            return;
        }
        var players = game.players;

        var playerIndex = getPlayerIdx(players, req.sessionID);

        var currPlayer = players[playerIndex];
        if (currPlayer && currPlayer.timeoutId){
            clearTimeout(currPlayer.timeoutId);
        }

        if (playerIndex === Number(game.currPlayer)){
            req.on('data', (data)=>{
                sentence += data;
            });

            req.on('end', ()=>{
                sentence = players[playerIndex].playerName + ": " + sentence;
                game.story.push(sentence);
                // Advance to next player
                var nextPlayerIdx =
                    (Number(game.currPlayer) + 1) % players.length;
                redis_driver.addSentence(gameName, sentence, nextPlayerIdx);
                game.currPlayer = nextPlayerIdx;
                initTimeout(gameName, players[nextPlayerIdx]);
                res.sendStatus(200).end();
            });
        } else if (-1 === playerIndex){
            res.status(404);
            res.sendFile(INDEX_HTML_PATH);
        }
    }));

    /**
     * Forget about the client. It the game is empty, delete it also.
     */
    app.post('/leavegame', LEX.createAcmeResponder(lex, (req, res)=>{
        var sess = req.session;
        var gameName = sess.gameName;

        leaveGame(gameName, req.sessionID);

        res.sendFile(INDEX_HTML_PATH);
    }));

    /**
    * Shut down the machine
    */
    app.post('/kill', (req, res)=>{
        child_process.exec('/sbin/shutdown 0', (error) => {
            if (error) {
                console.error(`exec error: ${error}`);
                res.send(`exec error: ${error}`);
            } else {
                res.send("Killing the VM\n");
            }
	    });
    });

});



//============================Helper functions==================================

/**
 * Send a HTML to the client
 * @param fileName the html path
 * @param res the response object to the client
 */
var sendHtml = (fileName, res)=>{
    "use strict";
    res.sendFile(fileName, (err)=>{
        if (err){
            console.log(err);
            res.status(err.status).end();
            return false;
        }
    });
};

/**
 * Return the player's index from the game's players list
 * @param players the list of players
 * @param sessionID the player session ID
 * @returns {number} the player index
 */
var getPlayerIdx = (players, sessionID)=>{
    "use strict";

    for (var iPlayer in players){
        if (players.hasOwnProperty(iPlayer) &&
            players[iPlayer].sessionID === sessionID){
            return Number(iPlayer);
        }
    }
    return -1;
};

/**
 * Get the game story array and return one string story
 * @param gameStory the game story array
 * @returns {string} all story at one string
 */
var makeStory = (gameStory)=>{
    "use strict";
    var res = "Story:";
    for (var sentence in gameStory){
        if (gameStory.hasOwnProperty(sentence)){
            res += ("\n" + gameStory[sentence]);
        }
    }
    return res;
};

/**
 * Player leave a game
 * @param gameName the game to leave
 * @param sessionID sessionID of the player
 */
var leaveGame = (gameName, sessionID)=>{
    "use strict";
    var game = games[gameName];
    if (!game){
        return;
    }
    var players = game.players;
    if (!players){
        return;
    }
    var indexOfPlayer = getPlayerIdx(players, sessionID);

    if (-1 !== indexOfPlayer){
        players.splice(indexOfPlayer, 1);
        redis_driver.removePlayer(gameName, sessionID);
        if (0 < players.length &&
            Number(game.currPlayer) === indexOfPlayer){
            game.currPlayer =
                (Number(game.currPlayer) + 1) % game.players.length;
            initTimeout(gameName, players[game.currPlayer]);
        }
    }
    if (0 === players.length){
        delete games[gameName];
        redis_driver.removeGame(gameName);
    }
};

/**
 * Set a timeout of 2 minutes to the player
 * @param gameName the game's name
 * @param player the player
 */
var initTimeout = (gameName, player)=>{
    "use strict";

    if (!player){
        return;
    }
    player.timeoutId = setTimeout(()=>{
        leaveGame(gameName, player.sessionID);
        console.log(player.playerName + " had been deleted");
    }, PLAYER_TIME_OUT_MILLISEC);

};
//===============================Constructors===================================
var Player = function(sessionID, playerName){
    "use strict";
    this.sessionID = sessionID;
    this.playerName = playerName;
};

var Game = function(storySize){
    "use strict";
    //this.story   = []; We add it dynamically
    //this.players = []; We add it dynamically
    this.maxStorySize = Number(storySize);
    this.currPlayer   = 0;
};

exports.Game = Game;

exports.Player = Player;

//==================================Kill========================================
