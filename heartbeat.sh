#!/bin/bash

while true
do
        pingres="$(ping 192.168.0.16 -c 1)"
        echo "$pingres"
        zeroreceived="0 received"
        if [[ $pingres =~ $zeroreceived ]]; then
                echo "Machine is down, starting the machine...";
                source /home/ibmcloud/rcFile # we have changed the rcFile to NOT ask for password
                /usr/local/bin/nova start TheSentenceGameVM
                sleep 10
        else
                echo "Machine is up";
        fi
        sleep 5
done


